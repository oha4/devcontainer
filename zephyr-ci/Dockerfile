FROM ubuntu:22.04
LABEL org.opencontainers.image.authors="albert.krenz@mailbox.org"

ARG ZEPHYR_VERSION=v3.5.0
ARG ZEPHYR_SDK_VERSION=0.16.3
LABEL version="$ZEPHYR_VERSION"

RUN apt-get update && apt-get install -y cargo cmake gcc g++ git linux-headers-generic ninja-build python3 python3-pip sudo wget gcc-multilib g++-multilib && rm -rf /var/lib/apt/lists/*

# ARG UID=1000
# ARG GID=1000
# RUN groupadd -g $GID -o user
# RUN useradd -u $UID -m -g user -G plugdev user \
# 	&& echo 'user ALL = NOPASSWD: ALL' > /etc/sudoers.d/user \
# 	&& chmod 0440 /etc/sudoers.d/user

RUN pip3 install west
RUN mkdir -p /opt/zephyrproject/

# USER user
RUN mkdir -p /opt/zephyrproject/ && \
    west init -m https://github.com/zephyrproject-rtos/zephyr --mr $ZEPHYR_VERSION /opt/zephyrproject && \
    cd /opt/zephyrproject && \
    west update

RUN pip3 install -r /opt/zephyrproject/zephyr/scripts/requirements.txt

RUN cd /opt/zephyrproject && \
    west zephyr-export

WORKDIR /opt/zephyrproject
RUN wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v$ZEPHYR_SDK_VERSION/zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64_minimal.tar.xz && wget https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v$ZEPHYR_SDK_VERSION/sha256.sum && \
    echo "$(cat sha256.sum | grep zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64_minimal.tar.xz)" | sha256sum -c - && \
    tar xvf zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64_minimal.tar.xz && cd zephyr-sdk-$ZEPHYR_SDK_VERSION && ./setup.sh -t arm-zephyr-eabi && ./setup.sh -t x86_64-zephyr-elf && cd .. && rm zephyr-sdk-${ZEPHYR_SDK_VERSION}_linux-x86_64_minimal.tar.xz && rm sha256.sum

ADD harness.py.patch /opt/zephyrproject/zephyr/
RUN cd /opt/zephyrproject/zephyr/ && patch -u scripts/pylib/twister/twisterlib/harness.py -i harness.py.patch && rm harness.py.patch
ENV ZEPHYR_BASE="/opt/zephyrproject/zephyr"
ENV ZEPHYR_SDK_INSTALL_DIR=/opt/zephyrproject/zephyr-sdk-$ZEPHYR_SDK_VERSION/

CMD ["/bin/bash"]
